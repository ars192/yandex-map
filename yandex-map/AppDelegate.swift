//
//  AppDelegate.swift
//  yandex-map
//
//  Created by Arystan on 3/31/21.
//

import UIKit
import YandexMapKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    // my key: "0a5906ca-0efa-477a-9cee-55e24678c887"
    let API_KEY = "c4d8ffe6-c93f-4f23-ac7a-89362bd75d36"
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        YMKMapKit.setApiKey(API_KEY)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let nav = UINavigationController(rootViewController: ViewController())
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
        return true
    }

}

